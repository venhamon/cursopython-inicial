from datetime import datetime

class Persona(object):

    def __init__(self, Nombre, Dni, FechaNac):
        self.Nombre = Nombre
        self.Dni = Dni
        self.FechaNac = FechaNac

    def edad_entero(self):
        return datetime.now().year - datetime.strptime(self.FechaNac, '%d/%m/%Y').year

    def edad_decimal(self):
        return (datetime.now() - datetime.strptime(self.FechaNac, '%d/%m/%Y')).days / 365


class Alumno(Persona):

    def __init__(self, Nombre, Dni, FechaNac):
         super().__init__(Nombre, Dni, FechaNac)

    def edad_mod(self):
        _now = datetime.now()
        import ipdb; ipdb.set_trace()
        Birth = datetime.strptime(self.FechaNac, '%d/%m/%Y')
        _edad = _now - Birth 
        return _edad.days / 365


persona = Persona('Leonardo', 34, '01/06/1989')
print('Hola {0} tu documento es {1}, naciste el {2} y es tu edad es: {3} anos'.format(
    persona.Nombre, persona.Dni, persona.FechaNac, persona.edad_decimal()))

alumno = Alumno('Leonardo', 34, '01/06/1989')
print('Hola {0} tu documento es {1}, naciste el {2} y es tu edad es: {3} anos'.format(
    alumno.Nombre, alumno.Dni, alumno.FechaNac, alumno.edad_mod()))