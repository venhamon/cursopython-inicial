import time

def test(pathfile):
    reader = open(pathfile, "w")
    if (reader.writable()):
        i = 0
        while(i < 5):
            reader.write("Val i: {0}\n".format(i))
            i += 1

def clase_write():
    archivo = open("prueba.txt", "w")
    archivo.write("Hola.\nBienvenidos al curso de Python.\nEsperemos que sea una agradable experiencia")
    print(archivo.tell())
    print(type(archivo))
    archivo.close()

def clase_read():
    archivo = open("prueba.txt", "r")
    print(archivo.readline())
    print(archivo.readlines())
    archivo.close()

def clase_readfor():
    archivo = open("prueba.txt", "r")
    archivo.seek(12)
    for linea in archivo.readlines():
        print(linea)
    archivo.close()


def copy_text(pathfile):
    archivo_origen = open(pathfile, "r")
    archivo_destino = open(pathfile + "_copy", "w")
    start_t = time.time()
    for linea in archivo_origen.readlines():
        archivo_destino.write(linea)
    end_t = time.time()
    archivo_origen.close()
    archivo_destino.close()
    print('Tiempo: {0}'.format(end_t - start_t))

def copy_text2(pathfile):
    archivo_origen = open(pathfile, "r")
    archivo_destino = open(pathfile + "_copy", "w")
    start_t = time.time()
    archivo_destino.write(archivo_origen.read())
    end_t = time.time()
    archivo_origen.close()
    archivo_destino.close()
    print('Tiempo: {0}'.format(end_t - start_t))