''' Ejemplo 1'''
from FirstCode import make_fibonacci
make_fibonacci(20)

print(make_fibonacci.__doc__)
print(make_fibonacci.__annotations__)

''' Ejemplo 2'''
from FirstCode import foo
foo(0)

''' Ejemplo 3'''
from FirstCode import muchos_items
muchos_items('.','primero','segundo', 'tercero')

''' Ejemplo 4'''
from FirstCode import muchos_items_palabras
muchos_items_palabras(nombre='pablo', apellido='back')

''' Ejemplo 5'''
from FirstCode import es_mayora_100
print(es_mayora_100(200))
print(es_mayora_100(60))

Va = int(input('ingrese un numero: '))
print(es_mayora_100(Va))