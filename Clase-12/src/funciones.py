from peewee import *
from datetime import date


db = SqliteDatabase('people.db')

class Person(Model):
    name = CharField()
    birthday = DateField()

    class Meta:
        database = db

class Pet(Model):
    name = CharField()
#    birthday = DateField()

    class Meta:
        database = db


# Conectar a la BD
def conectar(bd_name = ''):
    if(bd_name != ''):
        db.name = bd_name
    if (db.connect()):
        print('Base de datos conectada')

# Crea la tabla en la BD
def crear_tabla_person():
    if (db.table_exists(Person)):
        db.create_tables(Person)
        print('Tabla [person] creada')
    else:
        print('La tabla [person] ya existe')

def crear_tabla_pet():
    if (db.table_exists(Pet)):
        db.create_tables(Pet)
        print('Tabla [pet] creada')
    else:
        print('La tabla [pet] ya existe')

# Guardo contenido en la tabla dentro de la BD
def guardar_datos_tabla():
    uncle_bob = Person(name = 'Bob', birthday = date(1960, 1, 15))
    uncle_may = Person(name = 'May', birthday = date(1962, 8, 13))
    uncle_bob.save()
    uncle_may.save()

# Crear Registros
def crear_registros():
    grandma = Person.create(Person(name = 'Grandma', birthday = date(1935, 3, 1)))
    herb = Person.create(Person(name = 'Herb', birthday = date(1950, 5, 5)))

# Actualiza un dato especifico
def actualizar_registros():
    grandma.name = 'Grandma L.'
    grandma.save()

# Borra un registro
def borrar_registro():
    herb_mittens.delete_instance()

# Select + Where (Busco registros)
def traer():
    grandma = Person.select().where(Person.name == 'Grandma L.').get()

# Trae lista de registros
def traer_un_registro():
    query = Pet.select().where(Pet.animal_type == 'Cat')
    for pet in query:
        print(pet.name, pet.owner.name)

# union de dos listas distintas
def join():
    query(Pet
        .select(Pet, Person)
        .join(Person)
        .where(Pet.animal_type == 'cat'))

    for pet in query:
        print(pet.name, pet.owner.name)

# Order By
def order_by():
    for Person in Person().order_by(per.birthday):
        print(Person.name, Person.birtday)

# Where compleja
def where_compleja():
    d1940 = date(1940, 1, 1)
    d1960 = date(1960, 1, 1)
    query = (Person
            .select()
            .where((Person.birthday < d1940 | Person.birthday > d1960)))

    for Person in query:
        print(Person.name, Person.birtday)

