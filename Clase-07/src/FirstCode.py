''' Ejemplo 1'''
def make_fibonacci(limit, vervbose=False) -> list:
    """
    Genera la serie de Fibonacci hasta n.
    Parametro:
        limit: Hasta el numero que se debe generar.
        vervose: Si debe imprimir la serie.
    """
    a = 0
    b = 1
    result = []
    while a < limit:
        if vervbose:
            print(a, end=", ")
        result.append(a)
        a, b = b, a+b
    if vervbose:
        print()
    return result

make_fibonacci(200)

''' Ejemplo 2'''
def foo(limite, nombre='nada'):
    print('{0} - {1}'.format(limite, nombre))

''' Ejemplo 3'''
def muchos_items(separador, *args):
    print(separador.join(args))

''' Ejemplo 4'''
def muchos_items_palabras(**kwargs):
    for key in kwargs.keys():
        print('{0}: {1}'.format(key, kwargs[key]))

''' Ejemplo 5'''
def es_mayora_100(numero) -> bool:
    return numero > 100